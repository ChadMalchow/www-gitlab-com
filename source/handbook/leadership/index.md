## Behaviour

- As a leader new team members will look up to you and will follow your behaviour.
- Behaviour should be consistent inside and outside the company, don't fake it outside, just do the right thing inside the company as well.

## Books to read

- High output management - Andrew Grove
- The Hard thing about hard things - Ben Horowitz
- [The score takes care of itself - Bill Walsh](http://coachjacksonspages.com/The%20Score%20Takes%20Care.pdf)

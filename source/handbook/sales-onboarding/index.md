---
layout: markdown_page
title: "Sales Onboarding"
---

Every salesperson who starts at GitLab will enter an intense 30-day sales bootcamp.  The weekly class schedule can be found at [GitLab University](https://about.gitlab.com/university/).  In addition, once a new salesperson starts, their manager will create an issue for each of the first 4 weeks, tracking the progress of the new hire.

### Week 1 Test
1. [Version Control Systems Test](http://goo.gl/forms/8H8SNcH70T)
1. [Intro to Git Test](http://goo.gl/forms/GgWF1T5Ceg) 
1. Create a group on GitLab.com named: "glu_yourname"
1. Add your direct manager and Chad Malchow as master to the group
1. Create a project "About Me" with a README.md with a list of things that we should know about you
1. Create a project "90 day plan" with a README.md with what you plan to do in your first 90 days
1. Create a MR adding something funny to either of the projects and assign to your manager and Chad Malchow to it
1. Create an issue named: "Add me to TrainTool" and assign to Chad Malchow and tag your direct manager

### Week 2 Test
1. [Ecosystem Test](http://goo.gl/forms/5Vrf3CE0iC)
1. Complete TrainTool Course - GitLab vs. The Competition
1. Complete TrainTool Course - Jira Support
1. Complete TrainTool Course - Selling EE to CE User
1. Complete TrainTool Course - On-Premised vs. SaaS

### Week 3 Test
1. [GitLab 8.2 Test](http://goo.gl/forms/9PnmhiNzEa) 
1. [Big Files in Git Test](http://goo.gl/forms/RFsNK9fKuj) 
1. Complete TrainTool Course - Asking Effective Questions
1. Complete TrainTool Course - Closing Techniques
1. Complete TrainTool Course - Handling Pricing Objections
1. Record a demo using WebEx and send to your manager and Chad Malchow.  Demo scenario is an organization with 500 seats who uses Jira and BitBucket for a few teams, subversion within a couple teams and GitLab CE is also installed.

### Week 4 test
1. Create Fake Lead in Salesforce.  Lead First Name - Test; Lead Last Name - GitLab Bootcamp.  Company: GitLab Sales Bootcamp. Lead Source: Other
1. Convert Lead into an opportunity and Account
1. Ensure Opportunity follows naming convention process
1. Add Opportunity Product to Opportunity.  Quantity 2 of 100 seat Standard, one year.
1. Change close date to be 90 days out
1. Opportunity Stage to presenting solution

#### Supporting Information

1. [Our Sales Process](https://about.gitlab.com/handbook/sales-process/)

1. [Sales Best Practices Training](https://about.gitlab.com/handbook/sales-training/)

1. [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit)

1. [Recorded Practice Sales Demos](https://drive.google.com/drive/u/0/folders/0B_XVovPbWgADM1M3VUg1ZVJ0UjQ)
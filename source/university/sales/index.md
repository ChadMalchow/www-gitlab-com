---
layout: markdown_page
title: University | Sales Path
---

## Sales Boot Camp

**Goal:** Prepare new salespeople at GitLab

Each week there are learning goals and content to support the learning of the salesperson.
The goal of this boot camp is to have every salesperson prepared to help buyers navigate
the purchase of GitLab EE.

The General Boot Camp is divided in stages that correlate to the `Weeks` schedule
mentioned in here. Work on one stage per week.

Always start with the [General Boot Camp](../#general-boot-camp) and then work
your way here for more advanced and specific training.

### Week 1

Follow the [General Boot Camp](../#general-boot-camp) and concentrate on it
during your first week.

### Week 2

**Communicate the key differentiators between GitLab
  offerings and GitLab competition**

1. Understanding DevOps?
    - [Understanding DevOps](https://youtu.be/HpZBnc07q9o)
    - [DevOps at IBM](https://www.youtube.com/user/IBMRational)
    - [DevOps Where To Start](https://www.youtube.com/watch?v=CSrKwP1QrjE)
    - [Agile & DevOps](https://www.youtube.com/watch?v=WqoVeGFjK9k)
    - [Problem Solving with DevOps](https://www.youtube.com/watch?v=pTq9hFBWPeM)

### Week 3

**Learn how to pitch GitLab Enterprise Edition focused on the
  key selling points of EE.**

1. [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

1. [Client Demo of GitLab with Job and Haydn](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=ae7b72c61347030e8aa75328ed4b8660)

1. [Sales Onboarding materials](/handbook/sales-onboarding/)

### Week 4

**Create a lead, check if lead is using CE, create a task, convert
  lead to opportunity, add a product to the opportunity, create a quote, close
  win the opportunity, create a renewal opportunity and invoice the client.**

1. Review in order, learning material on our [Sales Procss](https://about.gitlab.com/handbook/sales-process/) page.

1. Login to [Salesforce.com](http://www.salesforce.com/), you should receive an
   email asking you to change your password.  Once you are in Salesforce, please
   familiarize yourself wth these reports/views as they will be critical in helping
   you manage your business.

    - [Your Current Month Pipeline](https://na34.salesforce.com/00O61000001uYbM) This view is to focus you on what you are committing to closing this month.  Are you in the right stage? What is needed to advance the sale to the next stage?
    - [Your Total Pipeline](https://na34.salesforce.com/00O61000001uYbR) This view should be used to identify where you are going and where you need focus to ensure you are successful.  What needs to to close and/or where you need to build up your pipleine - new business, expansion, add-on
    - [Your Leads](https://na34.salesforce.com/00Q?fcf=00B610000027qT9&rolodexIndex=-1&page=1) This view should be used to make sure you are followign up on each lead in a timely manner and have a plan on how you qualify or disqualify a lead.
        * [Your Personal Dashboard](https://na34.salesforce.com/01Z61000000J0gx) This dashboard should be used to understand where you have been, where you are at, where are you going and do you have the pipeline to get to where you need to be.
        * [Accounts you Own](https://na34.salesforce.com/001?fcf=00B61000001XPLz) This view is to be used to identify expansion opportunities, who you have neglected and the mix of customers to prospects you are working



[» Back](/university)